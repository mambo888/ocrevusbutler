import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from '@screens/Main/HomeScreen';
import AppointmentsScreen from '@screens/Main/Appointments';
import DocumentsScreen from '@screens/Main/Documents';
import TherapyDayScreen from '@screens/Main/TherapyDay';
import UberOcrevusScreen from '@screens/Main/UberOcrevus';
import EinstellungenScreen from '@screens/Main/Einstellungen';
import ChecklistScreen from '@screens/Main/Checklist';
import ArticleScreen from '@screens/Main/Article';

const MainNavigator = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: { header: null },
    },
    AppointmentsScreen: {
      screen: AppointmentsScreen,
      navigationOptions: { header: null },
    },
    DocumentsScreen: {
      screen: DocumentsScreen,
      navigationOptions: { header: null },
    },
    TherapyDayScreen: {
      screen: TherapyDayScreen,
      navigationOptions: { header: null },
    },
    UberOcrevusScreen: {
      screen: UberOcrevusScreen,
      navigationOptions: { header: null },
    },
    EinstellungenScreen: {
      screen: EinstellungenScreen,
      navigationOptions: { header: null },
    },
    ChecklistScreen: {
      screen: ChecklistScreen,
      navigationOptions: { header: null },
    },
    ArticleScreen: {
      screen: ArticleScreen,
      navigationOptions: { header: null },
    },
  },
  {
    initialRouteName: 'HomeScreen',
  },
);

const MainNavigation = createAppContainer(MainNavigator);
export default MainNavigation;