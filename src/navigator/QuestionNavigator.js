import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import StartScreen from '@screens/Question/Start';
import InputCodeScreen from '@screens/Question/InputCode';
import QuestionScreen from '@screens/Question/QuestionScreen';
import FinishScreen from '@screens/Question/FinishScreen';

const QuestionNavigator = createStackNavigator(
  {
    Start: {
      screen: StartScreen,
      navigationOptions: { header: null },
    },
    InputCodeScreen: {
      screen: InputCodeScreen,
      navigationOptions: { header: null },
    },
    QuestionScreen: {
      screen: QuestionScreen,
      navigationOptions: { header: null },
    },
    FinishScreen: {
      screen: FinishScreen,
      navigationOptions: { header: null },
    },
  },
  {
    initialRouteName: 'Start',
  },
);

const QuestionNavigation = createAppContainer(QuestionNavigator);
export default QuestionNavigation;
