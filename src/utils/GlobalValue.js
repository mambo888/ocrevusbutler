import { StyleSheet, Dimensions } from 'react-native';

const isDev = true; // true:develop, false:production

const { width, height } = Dimensions.get('window');
const ScreenHeight = width < height ? height : width; // height on portrait
const ScreenWidth = width < height ? width : height; // width on portrait

const AppColors = {
  C_PERIWINKLE: '#5F6EB3', // background color, text color
  C_PERIWINKLE_LIGHT: '#818bbd', // Q1 ~Q6 top background color
  C_PERIWINKLE_MORELIGHT: '#c7cde5', // Q1 yes no buttons
  C_YELLOW: '#FFC72A',
  C_GRAYBACK: '#EDEDEE',
  C_YELLOWGREEN: '#B5BE00',
  C_BTNTEXT: '#30354d',
  C_SUBTEXT: '#667C96',
};

const AppFonts = {
  F_Avenir_Reg: 'AvenirNextLTPro-Regular',
  F_Avenir_Bold: 'AvenirNextLTPro-Bold',
  F_Avenir_Demi: 'AvenirNextLTPro-Demi',
};

const GlobalValue = {
  Scale: ScreenWidth / 380,
  AnswerItemWidth: Math.round(ScreenWidth * 0.23),
  Months: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ],
  // ® \u24C7
};

const GlobalStyles = StyleSheet.create({});

export default {
  isDev,
  ScreenWidth,
  ScreenHeight,
  ...AppColors,
  ...AppFonts,
  ...GlobalValue,
  ...GlobalStyles,
};
