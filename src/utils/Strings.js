
const Strings = {
    Start: "LOS GEHTS!",
    Continue: "WEITER GEHTS",
    Finish: "FRAGEN ABSCHLIESSEN",
    Weiter: "WEITER",
    ShowApp: "ZEIGE MIR DIE APP",

    StartMessage: "Ich bin Dein Dein persönlicher Begleiter für eine optimale Vorbereitung auf Deinen OCREVUS(R)-Therapietag.\n\nBevor wir starten, müssen wir noch ein paar Dinge erledigen. Aber keine Sorge: ich unterstütze Dich dabei!",
    CodeConfirm: "Bitte gib hier Deinen persönlichen Registrierungs-Code aus Deinem Willkommenspaket ein",
    CodeWrong: "Du kannst diese App nur nutzen, wenn Dein Arzt für Dich eine OCREVUS(R)-Therapie vorgesehen hat. Hierfür erhältst Du von Deinem Arzt ein persönliches Willkommenspaket. Dieses beinhaltet Deinen persönlichen Zugangscode",
    FinishMessage: "Schön, dass Du Dich von mir auf dem Weg zu Deinem Therapietag begleiten lässt. Von nun an werde ich Dich über die nächsten Wochen unterstützen, damit Du für Deine Ocrevus®-Infusion optimal vorbereitet bist.\n\nNutze mich, um Termine für Deine Vorbereitungs-Untersuchungen einzutragen und viele nützliche Dinge über Ocrevus® zu erfahren.\n\nAls Dein persönlicher Assistent bin ich immer an Deiner Seite!",
    AddNew: "TERMIN EINTRAGEN",
    NotificationAlarm: "Damit Du auf dem Laufenden bleibst und nichts vergisst, möchte ich Dich gerne an Deine anstehenden Termine erinnern. Bist Du damit einverstanden?",

};

export default Strings;