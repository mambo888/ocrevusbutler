import AsyncStorage from '@react-native-community/async-storage';

export function saveData(key, value) {
  try {
    AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log('error', e);
  }
}
export async function getData(key) {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
  } catch (e) {
    console.log('error', e);
  }
}

// test functions
export function deleteAllData() {
  try {
    AsyncStorage.clear();
  } catch (e) {
    console.log('error', e);
  }
}
export function deleteOneData(key) {
  try {
    AsyncStorage.removeItem(key);
  } catch (error) {
    console.log('error', error);
  }
}
export async function getAnswers() {
  try {
    let answers = [];
    for (let i = 0; i < 6; i++) {
      const ref = 'a' + (i + 1).toString();
      let a = await AsyncStorage.getItem(ref);
      a = a || '';
      answers.push(a);
    }
    return answers;
  } catch (error) {
    console.log('error', error);
  }
}
export async function getAllData() {
  try {
    const savedScreens = await AsyncStorage.getItem('savedScreens');
    const a1 = await AsyncStorage.getItem('a1');
    const a2 = await AsyncStorage.getItem('a2');
    const a3 = await AsyncStorage.getItem('a3');
    const a4 = await AsyncStorage.getItem('a4');
    const a5 = await AsyncStorage.getItem('a5');
    const a6 = await AsyncStorage.getItem('a6');
    const QEnd = await AsyncStorage.getItem('QEnd');
    const sawInstruction = await AsyncStorage.getItem('sawInstruction');
    const acceptAlarm = await AsyncStorage.getItem('acceptAlarm');
    
    // console.log('---------------storage-----------');
    // console.log('savedScreens', savedScreens);
    // console.log('a1', a1);
    // console.log('a2', a2);
    // console.log('a3', a3);
    // console.log('a4', a4);
    // console.log('a5', a5);
    // console.log('a6', a6);
    // console.log('QEnd', QEnd);
    // console.log('sawInstruction', sawInstruction);
    // console.log('acceptAlarm', acceptAlarm);
  } catch (e) {
    console.log('error', e);
  }
}

// storeData = async () => {
//     try {
//       await AsyncStorage.setItem('@storage_Key', 'stored value')
//     } catch (e) {
//       // saving error
//     }
//   }
// getData = async () => {
//     try {
//         const value = await AsyncStorage.getItem('@storage_Key')
//         if (value !== null) {
//             // value previously stored
//         }
//     } catch (e) {
//         // error reading value
//     }
// }
