import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
const MenuIconWidth = '35rem'
const MenuIconHeight = '30rem'

const MenuIcon = ({ color, onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.menuicon_wrapper}>
        <View style={styles.menuicon}>
            <View style={[styles.menuicon_top, { backgroundColor: color }]} />
            <View style={[styles.menuicon_bottom, { backgroundColor: color }]} />
        </View>
    </TouchableOpacity>
);

export default MenuIcon;

const styles = EStyleSheet.create({
    menuicon_wrapper: {
        width: MenuIconWidth,
        height: MenuIconHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuicon: {
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    menuicon_top: {
        width: '18rem',
        height: '4rem',
    },
    menuicon_bottom: {
        marginTop: '4rem',
        width: '13rem',
        height: '4rem',
    },
});
