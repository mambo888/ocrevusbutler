import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Global from '@utils/GlobalValue';
import EStyleSheet from 'react-native-extended-stylesheet';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

const Height = Math.round(Global.ScreenHeight * 0.15);

const ArrowTextHeader = ({ title, onPress }) => (
    <View style={styles.container}>
        <Text style={styles.header_text}>{title}</Text>

        <View style={styles.arrow_wrapper}>

            <TouchableOpacity onPress={onPress} style={styles.arrow_btn} >
                <AntDesignIcon name="arrowleft" size={30} color={'white'} />
            </TouchableOpacity>

        </View>
    </View>
);

export default ArrowTextHeader;

const styles = EStyleSheet.create({
    container: {
        width: '100%',
        height: Height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Global.C_PERIWINKLE_LIGHT,
        paddingTop: 10,

        elevation: 20,
        shadowColor: 'gray',
        shadowOpacity: 0.5,
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 20,
    },
    header_text: {
        fontFamily: Global.F_Avenir_Demi,
        fontWeight: 'normal',
        fontSize: '16rem',
        color: 'white',
        letterSpacing: 1,
    },
    arrow_wrapper: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: '25%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red'
    },
    arrow_btn: {
        width: '50%',
        height: '50%',
        justifyContent:'center',
        alignItems: 'center'
    }
});
