import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import {
  saveData,
  getData,
  deleteOneData,
  getAllData,
  deleteAllData,
} from '@utils/GlobalFunction';
const guidetext = [
  'Dies ist Deine Zeitleiste bis zu Deinem Therapietag. Für eine optimale Vorbereitung musst Du noch ein paar Termine ausmachen. Drücke hierfür einfach auf die Buttons, um mehr zu erfahren.',
  'Dies ist Dein persönlicher Kalender. Trage ganz bequem Deine anstehenden Termine ein.',
  'Hier findest Du praktische Checklisten für Deine anstehenden Termine. Falls sich etwas ändern sollte, kannst Du Deine Termine hier jederzeit ändern.',
];
const ViewWidth = Math.round(Global.ScreenWidth * 0.7);
const ViewLeft = Math.round((Global.ScreenWidth - ViewWidth) / 2);

export default class InstructionView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stage: 1, // 1~3
    };
  }

  pressNext(isNext) {
    let nextStage;
    if (isNext) {
      nextStage = this.state.stage + 1;
    } else {
      nextStage = this.state.stage - 1;
    }
    if (nextStage > 3) {
      saveData('sawInstruction', '1');
      this.props.onFinish();
    } else {
      this.setState({
        stage: nextStage,
      });
    }
  }

  renderMainView() {
    const stage = this.state.stage;
    return (
      <View style={styles.mainview_wrapper}>
        {/* guide text */}
        <View style={styles.desc_wrapper}>
          <TextS1
            family="reg"
            size="14"
            color="white"
            label={guidetext[stage - 1]}
            textstyle={styles.desc_text}
          />
        </View>

        {/* next button */}
        <View style={styles.button_wrapper}>
          {/* {stage !== 1 &&
            <TouchableOpacity onPress={this.pressNext.bind(this, false)} style={{ width: '20%', height: '60%', justifyContent: 'center', alignItems: 'center' }}>
              <FontAwesome5Icon name="arrow-left" size={20} color={'white'} />
            </TouchableOpacity>
          } */}

          <TouchableOpacity
            onPress={this.pressNext.bind(this, true)}
            style={styles.button}>
            <TextS1
              family="bold"
              size="15"
              color={Global.C_PERIWINKLE}
              label={stage === 3 ? 'FINISH' : 'VERSTANDEN'}
              textstyle={styles.button_text}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderTriangle() {
    return <View style={styles.triangle}></View>;
  }

  render() {
    const stage = this.state.stage;
    let ViewBottom = 0;
    if (stage === 1) {
      ViewBottom = Math.round(Global.ScreenHeight * 0.13);
    } else if (stage === 2) {
      ViewBottom = Math.round(Global.ScreenHeight * 0.61 - ViewWidth);
    } else if (stage === 3) {
      ViewBottom = Math.round(Global.ScreenHeight - ViewWidth - 40);
    }

    return (
      <View style={styles.container}>
        <View style={[styles.subcontainer, { bottom: ViewBottom }]}>
          {/* triangle */}
          {stage === 2 && (
            <View style={{ top: 1, transform: [{ rotate: '180deg' }] }}>
              {this.renderTriangle()}
            </View>
          )}

          {this.renderMainView()}

          {/* triangle */}
          {stage !== 2 && (
            <View style={{ top: -1 }}>{this.renderTriangle()}</View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  triangle: {
    borderTopWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 0,
    borderLeftWidth: 10,
    borderTopColor: Global.C_PERIWINKLE,
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
  },
  container: {
    zIndex: 20,
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    backgroundColor: '#ffffff99',
    alignItems: 'center',
  },
  subcontainer: {
    position: 'absolute',
    width: ViewWidth,
    left: ViewLeft,
    flexDirection: 'column',
    alignItems: 'center',
  },
  mainview_wrapper: {
    width: '100%',
    aspectRatio: 1,
    backgroundColor: Global.C_PERIWINKLE,
    borderRadius: 5,
    flexDirection: 'column',
    alignItems: 'center',
  },
  desc_wrapper: {
    flex: 0.75,
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  desc_text: {
    textAlign: 'center',
    lineHeight: Math.round(Global.Scale * 17),
  },
  button_wrapper: {
    flex: 0.25,
    width: '80%',
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  button: {
    width: '80%',
    height: '60%',
    backgroundColor: 'white',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    textAlign: 'center',
    lineHeight: Math.round(ViewWidth * 0.15),
  },
});