import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import MenuIcon from '@component/MenuIcon';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
const dateIndicatorWidth = Math.round(Global.ScreenWidth * 0.17);
const dateIndicatorLeftMargin = 50;
const menuIconColor = '#667C96';
const ItemImages = [require('@images/book1.png'), require('@images/book2.png')];

export default class TherapyDay extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                {/* date selection rectangle absolute */}
                <View style={styles.date_wrapper}>
                    <View style={styles.f_03}></View>
                    <View style={styles.date}>
                        <TextS1 family="demi" size="18" color={'white'} label="05" />
                        <TextS1 family="demi" size="10" color={'white'} label="FEB" />
                    </View>
                </View>

                {/* menu icon absolute */}
                <View style={styles.menuicon_wrapper}>
                    <MenuIcon onPress={() => { }} color={menuIconColor} />
                </View>

                {/* title */}
                <View style={styles.title}>
                    <TextS0
                        family="reg"
                        size="18"
                        color={Global.C_BTNTEXT}
                        label={'Therapie Tag'}
                    />
                </View>

                {/* images */}
                <View style={styles.items}>
                    <TouchableOpacity style={styles.item}>
                        <Image
                            source={ItemImages[0]}
                            style={styles.wh100}
                            resizeMode="stretch"
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                        <Image
                            source={ItemImages[1]}
                            style={styles.wh100}
                            resizeMode="stretch"
                        />
                    </TouchableOpacity>
                </View>

                {/* back */}
                <View style={styles.bottomback_wrapper}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.goBack()}
                        style={styles.bottomback}>
                        <AntDesignIcon
                            name="arrowleft"
                            size={30}
                            color={Global.C_YELLOWGREEN}
                        />
                        <View style={styles.bottomback_image}>
                            <Image
                                source={require('@images/icon_therapy.png')}
                                style={styles.wh100}
                                resizeMode="contain"
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    date_wrapper: {
        position: 'absolute',
        left: dateIndicatorLeftMargin,
        top: 0,
        width: dateIndicatorWidth,
        aspectRatio: 0.8,
        backgroundColor: Global.C_YELLOWGREEN,
        borderRadius: 3,
        elevation: 20,
        shadowColor: 'gray',
        shadowOpacity: 0.5,
        shadowOffset: { width: 0, height: 5 },
        shadowRadius: 20,
        flexDirection: 'column',
    },
    menuicon_wrapper: {
        position: 'absolute',
        right: 30,
        top: dateIndicatorWidth - 20,
    },
    wh100: {
        width: '100%',
        height: '100%',
    },
    f_03: {
        flex: 0.3,
    },
    date: {
        flex: 0.7,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        marginTop: dateIndicatorWidth * 1.7,
        marginLeft: dateIndicatorLeftMargin,
    },
    items: {
        width: '90%',
        alignSelf: 'center',
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    item: {
        width: '45%',
        aspectRatio: 0.73,
    },
    bottomback_wrapper: {
        position: 'absolute',
        width: '100%',
        height: Math.round(Global.ScreenHeight * 0.13),
        left: 0,
        bottom: 0,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 30,
    },
    bottomback: {
        height: '40%',
        flexDirection: 'row',
        alignItems: 'center',
    },
    bottomback_image: {
        height: '100%',
        aspectRatio: 1,
        left: -5,
    },
});