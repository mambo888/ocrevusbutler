import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
const ItemImages = [
    require('@images/icon-drug.png'),
    require('@images/icon-blood.png'),
    require('@images/icon-pre.png'),
    require('@images/icon_therapy.png'),
];
const ImageWrapperWidth = Math.round(Global.ScreenHeight * 0.13);
// const ImageWidth = Math.round(ImageWrapperWidth / 2);

export default class TreatLineView extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const WrapperLeft = [0, 100, 250, Global.ScreenWidth - ImageWrapperWidth];

        return (
            <View style={styles.container}>
                <View style={styles.linewrapper}>
                    <View style={styles.line}></View>
                </View>

                <View style={styles.items_wrapper}>
                    {ItemImages.map((item, index) => {
                        return (
                            <View
                                key={index}
                                style={[styles.item_wrapper, { left: WrapperLeft[index] }]}>
                                <TouchableOpacity style={styles.item}>
                                    <Image
                                        source={item}
                                        style={styles.wh100}
                                        resizeMode="contain"
                                    />
                                </TouchableOpacity>
                            </View>
                        );
                    })}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    linewrapper: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    line: {
        width: '80%',
        height: 2,
        backgroundColor: Global.C_YELLOWGREEN,
    },
    items_wrapper: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    item_wrapper: {
        position: 'absolute',
        height: '100%',
        aspectRatio: 1,
        top: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    item: {
        height: '40%',
        aspectRatio: 1,
        backgroundColor: 'white',
    },
    wh100: {
        width: '100%',
        height: '100%',
    },
});