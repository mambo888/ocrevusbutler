import React from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Modal,
  ScrollView,
  FlatList,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import {
  saveData,
  getData,
  deleteOneData,
  getAllData,
  deleteAllData,
} from '@utils/GlobalFunction';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import RNRestart from 'react-native-restart';
import MenuIcon from '@component/MenuIcon';
import DateBar from './DateBar';
import InstructionView from './InstructionView';
import NotificationConfirmView from './NotificationConfirmView';
import AddNewView from './AddNewView';
import TreatLineView from './TreatLineView';
import MaterialCommunityIconsIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import RectShadowImage from '@component/RectShadowImage';
import Moment from 'moment';
import Strings from '@utils/Strings';
const ModalTop = Math.round(Global.ScreenHeight * 0.12);
const modalItems = [
  'Termine',
  'Dokumente',
  'Therapietag',
  'Über OCREVUS®',
  'Einstellungen',
  '--- Test Init ---',
];
const menuIconRightMargin = 30;
const AddNewBtnHeight = Math.round(Global.ScreenWidth * 0.12);
const ItemImages = [
  require('@images/inject.png'),
  require('@images/flask.png'),
];
const TestDataForInstruction = {
  type: 1,
  info: '201911100830', // 2019 Nov 10 08:30
  content:
    'Here’s a checklist of the required tests to be done before your treatment. Please fill it in with your doctor.',
};
const GoTodayHeight = 30;

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      isToday: true,
      listOfOneDay: [],
      displayDates: [], // array 3 '11/24/2019'
      datePickIndex: 0, // 0~2
      // loading: true,
      showInstruction: false,
      showAddNew: false,
      showNotificationConfirm: false,
    };
    this.onChangeDateIndex = this.onChangeDateIndex.bind(this);
    this.onChangeDateArrow = this.onChangeDateArrow.bind(this);
    this.onInstructionFinish = this.onInstructionFinish.bind(this);
    this.onNotificationConfirmFinish = this.onNotificationConfirmFinish.bind(
      this,
    );

    this.pressToday = this.pressToday.bind(this);
    this.onAddNewClose = this.onAddNewClose.bind(this);
  }

  async componentDidMount() {
    this.goToday();
    // instruction check
    const sawInstruction = await getData('sawInstruction');
    if (sawInstruction !== '1') {
      const testlist = [{ ...TestDataForInstruction }];
      this.setState({
        showInstruction: true,
        listOfOneDay: testlist,
      });
    }
  }

  goToday() {
    const today = Moment().format('L');
    const tomorrow = Moment()
      .add(1, 'days')
      .format('L');
    const aftertomorrow = Moment()
      .add(2, 'days')
      .format('L');
    this.setState({
      displayDates: [today, tomorrow, aftertomorrow],
      datePickIndex: 0,
      isToday: true,
    });
  }

  componentWillUnmount() {
    this.setState({ modalVisible: false });
  }

  pressModalItem(index) {
    // 0~5
    this.setState({ modalVisible: false });
    if (index === 0) {
      this.props.navigation.navigate('AppointmentsScreen');
    } else if (index === 1) {
      this.props.navigation.navigate('DocumentsScreen');
    } else if (index === 2) {
      this.props.navigation.navigate('TherapyDayScreen');
    } else if (index === 3) {
      this.props.navigation.navigate('UberOcrevusScreen');
    } else if (index === 4) {
      this.props.navigation.navigate('EinstellungenScreen');
    } else {
      // index === 5
      deleteAllData();
      RNRestart.Restart();
    }
  }

  goBackOnBoarding() {
    // test
    deleteAllData();
    RNRestart.Restart();
  }

  pressItemImage(item) {
    this.props.navigation.navigate('ChecklistScreen');
  }

  renderOneDayListItem(item) {
    return (
      <View style={styles.content_subwrapper}>
        <View style={styles.content_header}>
          <View style={styles.content_header_text}>
            <TextS1
              family="reg"
              size="10"
              color={Global.C_PERIWINKLE}
              label={'08.30 AM'}
            />
            <TextS0
              family="reg"
              size="15"
              color={'black'}
              label={'Blood test day'}
              textstyle={{ marginTop: 5 }}
            />
          </View>

          <View style={styles.content_header_pencilwrapper}>
            <TouchableOpacity style={styles.content_header_pencil}>
              <MaterialCommunityIconsIcon
                name="pencil"
                size={20}
                color={Global.C_PERIWINKLE}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.content_imagewrapper}>
          <View style={styles.content_imagesubwrapper}>
            <RectShadowImage
              onPress={() => this.pressItemImage(item)}
              width={Math.round(Global.ScreenWidth * 0.2688)}
              image={ItemImages[1]}
              backcolor={Global.C_PERIWINKLE}
            />
          </View>

          <View style={styles.content_textwrapper}>
            <TextS0
              family="reg"
              size="12"
              color={Global.C_PERIWINKLE}
              label={item.content}
              textstyle={styles.content_text}
            />
          </View>
        </View>
      </View>
    );
  }

  renderMenuModal() {
    return (
      <Modal
        visible={this.state.modalVisible}
        animationType={'fade'}
        onRequestClose={() => {
          this.state.modalVisible(false);
        }}
        transparent
        style={{ flex: 1 }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({ modalVisible: false });
          }}
          style={styles.modal_screenwrapper}>
          <View style={styles.modal_wrapper}>
            {modalItems.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  onPress={this.pressModalItem.bind(this, index)}
                  style={styles.modal_itemwrapper}>
                  <TextS1
                    family="demi"
                    size="14"
                    color={Global.C_SUBTEXT}
                    label={item}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }

  onChangeDateIndex(index) {
    // 0~2
    this.setState({
      datePickIndex: index,
      isToday: false,
    });
    const selectDate = this.state.displayDates[index];
    this.checkToday(selectDate);
  }

  onChangeDateArrow(isNext) {
    const currentFirstDate = this.state.displayDates[0]; // '11/23/2019'
    const strDate =
      currentFirstDate.substring(6, 10) +
      currentFirstDate.substring(0, 2) +
      currentFirstDate.substring(3, 5);
    const firstMomentDate = Moment(strDate, 'YYYYMMDD');
    let fdate;
    if (isNext) {
      fdate = firstMomentDate.add(3, 'days').format('L');
    } else {
      fdate = firstMomentDate.subtract(3, 'days').format('L');
    }
    const sdate = firstMomentDate.add(1, 'days').format('L');
    const tdate = firstMomentDate.add(1, 'days').format('L');
    this.setState({
      displayDates: [fdate, sdate, tdate],
    });
    const selectDate =
      this.state.datePickIndex === 0
        ? fdate
        : this.state.datePickIndex === 1
          ? sdate
          : tdate;
    this.checkToday(selectDate);
  }

  checkToday(date) {
    // '20191123'
    const today = Moment().format('L'); // '11/24/2019'
    if (today === date) {
      this.setState({
        isToday: true,
      });
    } else {
      this.setState({
        isToday: false,
      });
    }
  }

  onInstructionFinish() {
    this.setState({
      showInstruction: false,
      listOfOneDay: [],
    });

    // show notification confirm
    setTimeout(() => {
      this.setState({
        showNotificationConfirm: true,
      });
    }, 1000);
  }

  onNotificationConfirmFinish() {
    this.setState({
      showNotificationConfirm: false,
    });
  }

  pressToday() {
    this.goToday();
  }

  onAddNewClose() {
    this.setState({
      showAddNew: false,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        {/* instruction view */}
        {this.state.showInstruction && (
          <InstructionView onFinish={this.onInstructionFinish} />
        )}

        {/* NotificationConfirm view */}
        {this.state.showNotificationConfirm && (
          <NotificationConfirmView
            onFinish={this.onNotificationConfirmFinish}
          />
        )}

        {/* add new view */}
        {this.state.showAddNew && (
          <AddNewView
            isToday={this.state.isToday}
            onClose={this.onAddNewClose}
          />
        )}

        {/* menu modal */}
        {this.renderMenuModal()}

        {/* today back and menu icon */}
        <View style={styles.menuicon_wrapper}>
          {this.state.isToday ? (
            <View></View>
          ) : (
              // go today
              <TouchableOpacity
                onPress={this.pressToday.bind(this)}
                style={styles.gotoday_wrapper}>
                <EntypoIcon name="chevron-small-left" size={20} color={'white'} />
                <TextS0
                  family="demi"
                  size="10"
                  color={'white'}
                  label={'  HEUTE  '}
                  textstyle={{ lineHeight: GoTodayHeight }}
                />
              </TouchableOpacity>
            )}

          <View>
            <MenuIcon
              onPress={() => this.setState({ modalVisible: true })}
              color={'white'}
            />
          </View>
        </View>

        {/* today string */}
        {this.state.isToday ? (
          <View style={styles.todaytext_wrapper}>
            <TextS0 family="reg" size="22" color="white" label="Heute" />
            <TextS0
              family="reg"
              size="14"
              color="white"
              label="31 Tage bis zu Deiner Therapie"
              textstyle={styles.mt10}
            />
          </View>
        ) : (
            <View style={styles.todaytext_change}></View>
          )}

        {/* dates selection */}
        <View style={styles.datebar_wrapper}>
          <DateBar
            dates={this.state.displayDates}
            pickIndex={this.state.datePickIndex}
            onChangeDateIndex={this.onChangeDateIndex}
            onChangeDateArrow={this.onChangeDateArrow}
          />
        </View>

        {/* content area */}
        <View
          style={[
            styles.content_wrapper,
            { flex: this.state.isToday ? 0.43 : 0.51 },
          ]}>
          <View style={styles.h40}></View>

          {this.state.listOfOneDay.length > 0 && (
            <FlatList
              data={this.state.listOfOneDay}
              extraData={this.state}
              renderItem={({ item }) => this.renderOneDayListItem(item)}
              keyExtractor={(item, index) => index.toString()}
            />
          )}

          {/* no appointments */}
          {this.state.listOfOneDay.length === 0 && (
            <View style={styles.noappoint}>
              <TextS1
                family="demi"
                size="14"
                color={Global.C_SUBTEXT}
                label="Noch keine Termine"
              />
            </View>
          )}

          {/* add new */}
          <TouchableOpacity
            onPress={() => this.setState({ showAddNew: true })}
            style={styles.addnew_wrapper}>
            <TextS1
              family="demi"
              size="15"
              color="white"
              label={Strings.AddNew}
              textstyle={{ lineHeight: AddNewBtnHeight }}
            />
          </TouchableOpacity>
        </View>

        {/* spacer */}
        <View style={styles.f_005}></View>

        {/* points view */}
        <View style={styles.f_013}>
          <TreatLineView />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column',
  },
  addnew_wrapper: {
    marginTop: 30,
    alignSelf: 'center',
    width: '70%',
    height: AddNewBtnHeight,
    backgroundColor: Global.C_PERIWINKLE,
    borderRadius: 5,
    // elevation: 10,
    // shadowColor: 'gray',
    // shadowOpacity: 0.5,
    // shadowOffset: { width: 0, height: 5 },
    // shadowRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal_screenwrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#ffffff88',
  },
  modal_wrapper: {
    position: 'absolute',
    top: ModalTop,
    right: 40,
    backgroundColor: 'white',
    borderRadius: 5,
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 10,
    paddingHorizontal: 20,
    elevation: 10,
    shadowColor: 'gray',
    shadowOpacity: 0.8,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 20,
  },
  modal_itemwrapper: {
    // marginBottom: 20
    width: Math.round(Global.ScreenWidth * 0.35),
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
  },
  f_013: {
    flex: 0.13,
  },
  f_005: {
    flex: 0.05,
  },
  noappoint: {
    marginTop: 80,
    alignSelf: 'center',
  },
  content_wrapper: {
    backgroundColor: 'white',
    zIndex: 3,
    flexDirection: 'column',
  },
  h40: {
    height: 40,
  },
  datebar_wrapper: {
    flex: 0.13,
    zIndex: 5,
  },
  todaytext_wrapper: {
    flex: 0.14,
    backgroundColor: Global.C_PERIWINKLE_LIGHT,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 50,
  },
  mt10: {
    marginTop: 10,
  },
  menuicon_wrapper: {
    flex: 0.12,
    backgroundColor: Global.C_PERIWINKLE_LIGHT,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: menuIconRightMargin,
  },
  gotoday_wrapper: {
    height: GoTodayHeight,
    borderRadius: 5,
    borderColor: 'white',
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  content_subwrapper: {
    width: '100%',
    flexDirection: 'column',
  },
  content_header: {
    height: Math.round(Global.ScreenWidth * 0.12),
    paddingLeft: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  content_header_text: {
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  content_header_pencilwrapper: {
    height: '100%',
    paddingRight: 30,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  content_header_pencil: {
    height: '70%',
    aspectRatio: 1,
    backgroundColor: Global.C_GRAYBACK,
    borderRadius: Math.round(Global.ScreenWidth * 0.042),
    elevation: 10,
    shadowColor: 'gray',
    shadowOpacity: 0.5,
    shadowOffset: { width: 0, height: 5 },
    shadowRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content_imagewrapper: {
    paddingLeft: 50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  todaytext_change: {
    flex: 0.06,
    backgroundColor: Global.C_PERIWINKLE_LIGHT,
  },
  content_imagesubwrapper: {
    marginTop: 10,
    marginBottom: 20,
  },
  content_textwrapper: {
    marginTop: 20,
    width: Math.round(
      Global.ScreenWidth - Global.ScreenWidth * 0.2688 - 90,
    ),
  },
  content_text: {
    marginLeft: 15,
    textAlign: 'justify',
    lineHeight: 14,
  },
});