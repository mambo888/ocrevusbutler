import React from 'react';
import {
    View,
    TouchableOpacity,
    StyleSheet,
    ScrollView,
    FlatList,
} from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import { NavigationActions } from 'react-navigation';
import Global from '@utils/GlobalValue';
import ArrowTextHeader from '@component/ArrowTextHeader';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
const ChecklistData = [
    {
        // vaccination
        title: 'Checkliste für Impfungen',
        text: [
            'Hier findest Du alle benötigten Impfungen, die Du vor Deiner Therapie mit Ocrevus® machen musst. Du kannst zusammen mit Deinem Arzt die Impfungen markieren um sicherzustellen, dass Du nichts vergessen hast.\n',
            'Möchtest Du mehr zum Thema Impfung und OCREVUS® wissen?',
        ],
        info: [
            {
                category: 'IMPFSTATUS',
                items: [
                    'Grundimmunisierung nach STIKO Impfkalender\n(Tetanus, Diphterie, Pertussis, Poliomyelitis, Haemophilus influenzae Typ B, Rotaviren, Meningokokken, Masem, Mumps, Röteln, Verizellen)',
                    'Saisonale Influenza (jährlich)',
                    'Pneumokokken (falls noch nicht vorhanden)',
                    'Auffrischimpfung Tetanus / Diphterie (alle 10 Jahre)',
                    'Hepatitis B',
                ],
            },
        ],
    },
    {
        // blood test
        title: 'Checkliste für Bluttests',
        text: [
            'Hier findest Du alle Bluttests, die Du vor Deiner Therapie mit Ocrevus® machen musst. Du kannst zusammen mit Deinem Arzt alle Tests markieren, um sicherzustellen, dass Du nichts vergessen hast.\n',
            'Möchtest Du mehr über die Blutuntersuchungen wissen?',
        ],
        info: [
            {
                category: 'LABOR',
                items: [
                    'Blutbild mit Differentialblutbild',
                    'Gesamt lgG im Serum',
                    'Leukozyten - Subpopulationen',
                    'C-reaktives Protein (CRP)',
                    'Urinstatus',
                    'Schwangersschaftstest (bei Frauen im gebährfähigen Alter)',
                ],
            },
            {
                category: 'SEROLOGIE',
                items: [
                    'Hepatitis B',
                    'Hepatitis C',
                    'HIV',
                    'Varizella zoster Virus',
                    'Tuberkulose',
                    'JC-Virus (bei Vortherapie mit Natalizumab)',
                ],
            },
        ],
    },
];
const subtext1color = '#667c96';
const subtext2color = '#889dfb';
const CategoryHeight = Math.round(Global.ScreenWidth * 0.09);
const CircleHeight = Math.round(Global.ScreenWidth * 0.09);
const C_CIRCLE_BACK_DESELECT = '#dfe2f0';

const ArticleData = [
    {
        // vaccination
        image: require('@images/moreinfo_vaccination.png'),
        title: 'Impfen bei OCREVUS® – geht das?',
        desc:
            'Weder die MS noch die OCREVUS® Therapie verhindern, dass Du Dich impfen lassen kannst. Im Gegenteil: Ein guter Impfschutz ist für Dich besonders wichtig!',
        infolist: [
            {
                title: 'Warum überhaupt impfen lassen?',
                text:
                    'Verschiedene Infektionskrankheiten wie beispielsweise die Grippe können bei MS-Betroffenen Schübe auslösen. Zudem kann die Grippe bei ihnen einen schwereren Verlauf nehmen. Auch die Therapie mit OCREVUS® kann Dich anfälliger für Infektionen machen. Hintergrund ist die Wirkweise (interner LINK): Die fehlgeleiteten B-Zellen, die OCREVUS® aus dem Körper entfernt, sind auch an der Immunantwort beteiligt. Impfungen können Dich vor einer Ansteckung schützen. Die Behauptung, Impfungen könnten einen Schub oder gar MS auslösen, wird durch Studien widerlegt.',
            },
            {
                title: 'Besonderheiten beim Impfen unter OCREVUS®',
                text:
                    'Ein vor Therapiebeginn vom Immunsystem aufgebauter Schutz gegen Krankheitserreger bleibt unter OCREVUS® erhalten. Die immunmodulierende Wirkung von OCREVUS® kann jedoch dazu führen, dass Dein Körper während der Behandlung nicht ganz so gut auf Impfungen anspricht. Aber auch bei bereits begonnener Therapie kannst Du geimpft werden. So kannst Du beispielsweise weiterhin die jährliche Grippeimpfung erhalten. Denn selbst wenn die Impfantwort möglicherweise geringer ausfällt, kann sie das Infektionsrisiko reduzieren.',
            },
        ],
    },
    {
        // blood test
        image: '',
        title: 'Blutuntersuchungen bei OCREVUS®',
        desc:
            'Dein Arzt untersucht vor Beginn der Therapie mit OCREVUS® und dann vor jeder Infusion Dein Blut. Damit kann er feststellen, ob Du OCREVUS® sicher erhalten kannst.',
        infolist: [
            {
                title: 'Vor dem Therapiestart',
                text:
                    'Bevor Du mit der OCREVUS® Therapie beginnen kannst, prüft Dein Arzt an einer Blutprobe, ob Du an einer chronischen oder akuten Infektion mit bestimmten Viren oder Bakterien leidest (Hepatitis-B- oder C-Virus, HIV, Varizellen, Tuberkulose). Diese Untersuchung wird halbjährlich bzw. jährlich wiederholt. Um sicherzugehen, dass Du alle empfohlenen Impfungen bekommen hast, kann Dein Arzt vor Therapiestart auch Deinen Impfstatus anhand der Blutprobe bestimmen.',
            },
            {
                title: 'Vor jeder Infusion',
                text:
                    'Am Infusionstag solltest Du Dich gut fühlen. Leidest Du an einer akuten Infektion, sollte die Infusion verschoben werden, bis die Infektion abgeklungen ist. Deshalb untersucht Dein Arzt Dein Blut auf verschiedene Parameter, die Hinweise auf eine Infektion geben. An einer Blutprobe kann Dein Arzt also vor jeder Infusion ablesen, ob Du fit für die OCREVUS® Gabe bist.',
            },
        ],
    },
];

export default class Checklist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: [],
        };
    }

    renderHeader(category) {
        return (
            <View style={styles.itemheader}>
                <TextS1
                    family="demi"
                    size="14"
                    color={subtext1color}
                    label={category}
                    textstyle={{ lineHeight: CategoryHeight }}
                />
            </View>
        );
    }

    pressItem(index, subindex) {
        const pushobj = index.toString() + subindex.toString();
        let selected = this.state.selected;
        if (selected.includes(pushobj)) {
            for (var i = 0; i < selected.length; i++) {
                if (selected[i] === pushobj) {
                    selected.splice(i, 1);
                }
            }
        } else {
            selected.push(pushobj);
        }
        this.setState({
            selected: selected,
        });
    }

    renderItem(item, subindex, index) {
        const selected = this.state.selected;
        let isSelected = false;
        const checkobj = index.toString() + subindex.toString();
        if (selected.includes(checkobj)) {
            isSelected = true;
        }
        const check_backcolor = isSelected
            ? Global.C_PERIWINKLE
            : C_CIRCLE_BACK_DESELECT;
        return (
            <TouchableOpacity
                key={index}
                onPress={this.pressItem.bind(this, index, subindex)}
                style={[styles.item_one, { borderTopWidth: subindex === 0 ? 0 : 1 }]}>
                <View
                    style={[styles.check_wrapper, { backgroundColor: check_backcolor }]}>
                    <FontAwesomeIcon
                        name="check"
                        size={20}
                        color={isSelected ? 'white' : Global.C_PERIWINKLE_MORELIGHT}
                    />
                </View>

                <View style={styles.item_textwrapper}>
                    <TextS2
                        family="reg"
                        size="14"
                        color={Global.C_PERIWINKLE}
                        label={item}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    renderFooter() {
        return <View style={styles.footer}></View>;
    }

    pressSubText(type) {
        // 0:vaccination 1:bloodtest
        if (type === 0 || type === 1) {
            const navigateAction = NavigationActions.navigate({
                routeName: 'ArticleScreen',
                params: {
                    data: ArticleData[type],
                },
            });
            this.props.navigation.dispatch(navigateAction);
        }
    }

    render() {
        const { params } = this.props.navigation.state;
        const type = params.index - 1;
        const Checklistitem = ChecklistData[type];
        const title = Checklistitem.title;
        const subtext1 = Checklistitem.text[0];
        const subtext2 = Checklistitem.text[1];
        const infolist = Checklistitem.info;

        return (
            <View style={styles.container}>
                {/* header */}
                <ArrowTextHeader
                    title={title}
                    onPress={() => this.props.navigation.goBack()}
                />

                <ScrollView>
                    <View style={styles.desc1}>
                        <TextS0
                            family="demi"
                            size="14"
                            color={subtext1color}
                            label={subtext1}
                            textstyle={{ textAlign: 'center' }}
                        />
                    </View>

                    <TouchableOpacity
                        onPress={() => this.pressSubText(type)}
                        style={styles.desc2}>
                        <TextS0
                            family="demi"
                            size="14"
                            color={subtext2color}
                            label={subtext2}
                            textstyle={{ textAlign: 'center' }}
                        />
                    </TouchableOpacity>

                    {infolist.map((oneiteminfo, topindex) => {
                        return (
                            <View key={topindex} style={styles.item_wrapper}>
                                <FlatList
                                    data={oneiteminfo.items}
                                    renderItem={({ item, index }) =>
                                        this.renderItem(item, index, topindex)
                                    }
                                    keyExtractor={(item, index) =>
                                        topindex.toString() + index.toString()
                                    }
                                    ListHeaderComponent={this.renderHeader(oneiteminfo.category)}
                                    ListFooterComponent={this.renderFooter}
                                />
                            </View>
                        );
                    })}
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    check_wrapper: {
        width: CircleHeight,
        height: CircleHeight,
        borderRadius: Math.round(CircleHeight / 2),
        marginRight: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    item_textwrapper: {
        width: Math.round(Global.ScreenWidth * 0.95 - CircleHeight - 15),
    },
    container: {
        flex: 1,
    },
    desc1: {
        width: '80%',
        alignSelf: 'center',
        marginTop: 40,
    },
    desc2: {
        width: '60%',
        alignSelf: 'center',
        marginBottom: 40,
    },
    itemheader: {
        width: Math.round(Global.ScreenWidth * 0.95),
        height: CategoryHeight,
        backgroundColor: Global.C_GRAYBACK,
        justifyContent: 'center',
        alignItems: 'center',
    },
    item_one: {
        width: Math.round(Global.ScreenWidth * 0.95),
        paddingTop: 20,
        marginBottom: 15,
        flexDirection: 'row',
        borderTopColor: Global.C_GRAYBACK,
    },
    footer: {
        height: 30,
    },
    item_wrapper: {
        alignItems: 'center',
    },
});