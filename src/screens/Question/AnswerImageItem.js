import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import isEmpty from '@utils/isEmpty';
const QItemImages = [
    require('@images/Q1.png'),
    require('@images/Q2.png'),
    require('@images/Q3.png'),
    require('@images/Q4.png'),
    require('@images/Q5.png'),
    require('@images/Q6.png'),
];

export default class AnswerImageItem extends React.Component {
    // constructor(props) {
    //     super(props);
    // }

    renderContent() {
        const screenIndex = this.props.screenIndex;
        const data = this.props.data;
        if (isEmpty(data)) return;
        if (screenIndex === 1) {
            const month = Global.Months[
                parseInt(data.substring(4, 6)) - 1
            ].toUpperCase();
            const date = data.substring(6, 8);
            return (
                <View style={styles.a1wrapper}>
                    <View style={styles.a1month}>
                        <TextS0
                            family="bold"
                            size="8"
                            color={Global.C_PERIWINKLE}
                            label={month}
                        />
                    </View>

                    <View style={styles.a1date}>
                        <TextS1 family="demi" size="14" color={'white'} label={date} />
                    </View>
                </View>
            );
        } else if (screenIndex === 3) {
            let age = (parseInt(data) + 1).toString();
            if (age.length < 2) age = '0' + age;
            return (
                <View style={styles.a3wrapper}>
                    <View style={styles.a3age}>
                        <TextS0 family="demi" size="11" color={'white'} label={age} />
                    </View>
                </View>
            );
        } else if (screenIndex === 5) {
            // 201710
            const month = Global.Months[
                parseInt(data.substring(4, 6)) - 1
            ].toUpperCase();
            const year = "'" + data.substring(2, 4);
            return (
                <View style={styles.a5wrapper}>
                    <View style={styles.a5month}>
                        <TextS0 family="bold" size="9" color={'white'} label={month} />
                    </View>

                    <View style={styles.a5year}>
                        <TextS0 family="demi" size="11" color={'white'} label={year} />
                    </View>
                </View>
            );
        } else {
            return <View></View>;
        }
    }

    render() {
        const screenIndex = this.props.screenIndex;
        return (
            <View style={styles.wh100}>
                <Image
                    source={QItemImages[screenIndex - 1]}
                    style={styles.wh100}
                    resizeMode="contain"
                />

                {this.renderContent()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wh100: {
        width: '100%',
        height: '100%',
    },
    a1wrapper: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    a1month: {
        width: '50%',
        height: '15%',
        marginTop: '30%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    a1date: {
        width: '50%',
        height: '25%',
        marginTop: '5%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    a3wrapper: {
        position: 'absolute',
        width: '100%',
        height: '100%',
    },
    a3age: {
        width: '30%',
        height: '20%',
        marginTop: '30%',
        marginLeft: '50%',
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    a5wrapper: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        alignItems: 'center',
    },
    a5month: {
        width: '40%',
        height: '20%',
        marginTop: '36%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    a5year: {
        zIndex: 10,
        width: '40%',
        height: '20%',
        marginTop: '2%',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
