import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import isEmpty from '@utils/isEmpty';
import { getData } from '@utils/GlobalFunction';
// import LinearGradient from 'react-native-linear-gradient';
const lastage = 99;
let ages = [];
for (let i = 1; i <= lastage; i++) {
  let age = i.toString();
  if (i < 10) {
    age = '0' + age;
  }
  ages.push(age);
}

const OneNumberWidth = 50;

export default class Answer3Screen extends React.Component {
  constructor() {
    super();
    this.state = {
      pickerIndex: 29, // = 30
      loading: true,
    };
    this.ScrollViewRef = null;
  }

  componentDidMount = async () => {
    const date_str = await getData('a3');
    let pickerIndex = this.state.pickerIndex;
    if (!isEmpty(date_str)) {
      pickerIndex = parseInt(date_str, 10);
      this.setState({
        pickerIndex: pickerIndex,
      });
    }

    if (this.ScrollViewRef) {
      const initX = pickerIndex * OneNumberWidth + Math.round(OneNumberWidth / 2);
      setTimeout(() => {
        if (this.ScrollViewRef) {
          this.ScrollViewRef.scrollTo({ x: initX });
          this.setState({
            loading: false,
          })
        }
      }, 1);
    }
  }


  onScroll = event => {
    if (this.state.loading) return;
    const offsetX = event.nativeEvent.contentOffset.x;
    let centerAge = Math.ceil(offsetX / OneNumberWidth);
    // if (centerAge - 1 === this.state.pickerIndex) {
    //   return;
    // } else {
      if (centerAge < 1) {
        centerAge = 1;
      } else if (centerAge > lastage) {
        centerAge = lastage;
      }
      this.setState({
        pickerIndex: centerAge - 1,
      });
    // }
  };
  onMomentumScrollEnd = event => {
    if (this.state.loading) return;
    const offsetX = event.nativeEvent.contentOffset.x;
    let centerAge = Math.ceil(offsetX / OneNumberWidth);
    if (centerAge < 1) {
      centerAge = 1;
    } else if (centerAge > lastage) {
      centerAge = lastage;
    }
    this.setState({
      pickerIndex: centerAge - 1,
    });
    const data = {
      screen: 3,
      save: centerAge - 1,
    };
    this.props.onChanged(data);
  };

  render() {
    const oneNumberStyleObj = {
      width: OneNumberWidth,
      textAlign: 'center',
    };
    return (
      <View style={styles.container}>
        <View style={styles.subcontainer}>
          {/* <LinearGradient colors={['#4c669f22', '#3b599822', '#192f6a22']} style={{ position: 'absolute', width: '100%', height: '100%', left: 0, top: 0, backgroundColor: 'transparent' }}>
          </LinearGradient> */}

          <ScrollView
            ref={view => (this.ScrollViewRef = view)}
            horizontal={true}
            onScroll={this.onScroll}
            onMomentumScrollEnd={this.onMomentumScrollEnd}
            showsHorizontalScrollIndicator={false}
            style={styles.w100}
            contentContainerStyle={{ alignItems: 'flex-end' }}>
            <View style={styles.spacer} />

            {ages.map((item, index) => {
              // const fontSize = this.state.pickerIndex === index ? 40 : 18;
              // let opacity = 0;
              // const diffIndex = Math.abs(this.state.pickerIndex - index);
              // if (diffIndex < 6) {
              //   opacity = 1 - diffIndex * 0.15;
              // }
              return (
                <View key={index} style={{ opacity: 1 }}>
                  <TextS2
                    family="reg"
                    size="18"
                    color={Global.C_PERIWINKLE}
                    label={item}
                    textstyle={oneNumberStyleObj}
                  />
                </View>
              );
            })}

            <View style={styles.spacer} />
          </ScrollView>

          <View style={styles.picker_wrapper}>
            <TextS2
              family="bold"
              size="36"
              color={Global.C_PERIWINKLE}
              label={ages[this.state.pickerIndex]}
              textstyle={oneNumberStyleObj}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  spacer: {
    width: Math.round(Global.ScreenWidth / 2),
  },
  subcontainer: {
    width: '100%',
    height: 50,
    marginTop: 80,
  },
  w100: {
    width: '100%',
  },
  picker_wrapper: {
    position: 'absolute',
    width: 60,
    height: 50,
    bottom: 0,
    left: Math.round(Global.ScreenWidth / 2 - 20),
    backgroundColor: 'white',
    alignItems:'center'
  },
});
