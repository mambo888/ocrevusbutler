import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {TextS0, TextS1, TextS2} from '@component/Text';
import Global from '@utils/GlobalValue';

export default class Answer2Screen extends React.Component {
  constructor() {
    super();
    this.state = {
      gender: 0,
    };
  }
  pressGender(gender) {
    // 1~3
    this.setState({
      gender: gender,
    });
    const data = {
      screen: 2,
      save: gender, // 1:frau, 2:mann, 3:divers
    };
    this.props.onChanged(data);
  }

  render() {
    const gender = this.state.gender;
    return (
      <View style={styles.container}>
        
        {/* <View style={styles.spacer1} /> */}

        <View style={styles.topcontainer}>
          <View style={styles.frau_wrapper}>
            <TouchableOpacity
              onPress={this.pressGender.bind(this, 1)}
              style={{
                width: gender === 1 ? '100%' : '80%',
                height: gender === 1 ? '90%' : '70%',
              }}>
              <Image
                source={require('@images/Q2Frau.png')}
                style={styles.wh100}
                resizeMode="contain"
              />
            </TouchableOpacity>

            <TextS2
              family="reg"
              size="12"
              color={Global.C_PERIWINKLE}
              label="Frau"
              textstyle={{marginTop: 10}}
            />
          </View>

          <View style={styles.mann_wrapper}>
            <TouchableOpacity
              onPress={this.pressGender.bind(this, 2)}
              style={{
                width: gender === 2 ? '100%' : '80%',
                height: gender === 2 ? '90%' : '70%',
              }}>
              <Image
                source={require('@images/Q2Mann.png')}
                style={styles.wh100}
                resizeMode="contain"
              />
            </TouchableOpacity>

            <TextS2
              family="reg"
              size="14"
              color={Global.C_PERIWINKLE}
              label="Mann"
              textstyle={{marginTop: 10}}
            />
          </View>
        </View>

        <View style={styles.spacer2} />

        <View style={styles.bottomconatiner}>
          <TouchableOpacity
            onPress={this.pressGender.bind(this, 3)}
            style={{
              width: gender === 3 ? '100%' : '80%',
              height: gender === 3 ? '90%' : '70%',
            }}>
            <Image
              source={require('@images/Q2Divers.png')}
              style={styles.wh100}
              resizeMode="contain"
            />
          </TouchableOpacity>

          <TextS2
            family="reg"
            size="14"
            color={Global.C_PERIWINKLE}
            label="Divers"
            textstyle={{marginTop: 10}}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    // backgroundColor:'blue'
  },
  // item_text: {
  //   marginTop: 10,
  //   fontFamily: Global.F_Avenir_Reg,
  //   fontWeight: 'normal',
  //   fontSize: 14,
  //   color: 'white',
  // },
  wh100: {
    width: '100%',
    height: '100%',
  },
  topcontainer: {
    width: '80%',
    height: '45%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    // alignItems: 'flex-end',
    // backgroundColor:'green'
  },
  bottomconatiner: {
    // marginTop:'5%',
    width: '23%',
    height: '45%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    // backgroundColor:'green'
  },
  // spacer1: {
  //   height: '5%',
  // },
  spacer2: {
    height: '5%',
  },
  frau_wrapper: {
    width: '28%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    // backgroundColor:'red'
  },
  // item_imagewrapper: {
  //   width: '80%',
  //   height: '70%',
  // },
  mann_wrapper: {
    width: '32%',
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    // backgroundColor:'red'
  },
});
