import React from 'react';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';

export default class Answer4Screen extends React.Component {
  constructor() {
    super();
    this.state = {
      selection: 0,
    };
  }

  pressImage(index) {
    this.setState({
      selection: index,
    });
    const data = {
      screen: 4,
      save: index,
    };
    this.props.onChanged(data);
  }

  render() {
    const selection = this.state.selection;
    return (
      <View style={styles.container}>
        <View style={styles.spacer} />

        <View style={styles.topcontainer}>
          <View style={styles.topitem}>
            <TouchableOpacity
              onPress={this.pressImage.bind(this, 1)}
              style={{
                width: Math.round(Global.ScreenWidth * 0.3),
                height: selection === 1 ? '90%' : '70%',
                marginBottom: 5,
              }}>
              <Image
                source={require('@images/Q4Sch.png')}
                style={styles.wh100}
                resizeMode="contain"
              />
            </TouchableOpacity>

            <TextS2 family="reg" size="12" color={Global.C_PERIWINKLE} label="Schubförmige" />
            <TextS2 family="reg" size="12" color={Global.C_PERIWINKLE} label="(remetierende)" />
          </View>

          <View style={styles.topitem}>
            <TouchableOpacity
              onPress={this.pressImage.bind(this, 2)}
              style={{
                width: Math.round(Global.ScreenWidth * 0.3),
                height: selection === 2 ? '90%' : '70%',
                marginBottom: 5,
              }}>
              <Image
                source={require('@images/Q4Sek.png')}
                style={styles.wh100}
                resizeMode="contain"
              />
            </TouchableOpacity>

            <TextS2 family="reg" size="12" color={Global.C_PERIWINKLE} label="Sekundär" />
            <TextS2 family="reg" size="12" color={Global.C_PERIWINKLE} label="progrediente" />
          </View>
        </View>

        {/* <View style={styles.spacer} /> */}

        <View style={styles.bottomcontainer}>
          <TouchableOpacity
            onPress={this.pressImage.bind(this, 3)}
            style={{
              width: Math.round(Global.ScreenWidth * 0.3),
              marginBottom: 5,
              height: selection === 3 ? '63%' : '49%',
            }}>
            <Image
              source={require('@images/Q4Pri.png')}
              style={styles.wh100}
              resizeMode="contain"
            />
          </TouchableOpacity>

          <TextS2 family="reg" size="12" color={Global.C_PERIWINKLE} label="Primär" />
          <TextS2 family="reg" size="12" color={Global.C_PERIWINKLE} label="progrediente" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
  },
  topcontainer: {
    width: '80%',
    height: '45%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    // backgroundColor:'blue'
  },
  topitem: {
    // width: '40%',
    height: '70%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    // backgroundColor:'yellow'
  },
  bottomcontainer: {
    height: '45%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    // backgroundColor:'blue'
  },
  spacer: {
    height: '5%',
    // backgroundColor:'blue'
  },
  wh100: {
    width: '100%',
    height: '100%',
  },

  // item_wrapper: {
  //   width: '100%',
  //   height: '70%',
  //   marginBottom: 5,
  // },
  // item_text: {
  //   fontFamily: Global.F_Avenir_Reg,
  //   fontWeight: 'normal',
  //   fontSize: 14,
  //   color: 'white',
  // },
});
