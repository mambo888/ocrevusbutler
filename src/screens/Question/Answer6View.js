import React from 'react';
import { View, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import isEmpty from '@utils/isEmpty';
import { getData } from '@utils/GlobalFunction';
const items = [
  'Interferon',
  'Glatirameracetat (z.B.Copaxone®)',
  'Fingolimod (Gylenia®)',
  'Natalizumab (Tysabri®)',
  'Dimethylfumarat (Tecfidera®)',
  'Cladibrin (Mavenclad®)',
  'Mitoxantoron',
  'Alemtuzumab (Lemtrada®)',
  'Teriflunomid (Aubagio®)',
  'none',
];
const subTitle = '*Mehrfachnennung möglich';
const ItemHeight = 30;
// const C_CIRCLE_DESELECT = Global.C_GRAYBACK;
const C_CIRCLE_BACK_DESELECT = '#dfe2f0';
// const C_CHECK_SELECT = '#5CACAD';

// function convertUnicode(input) {
//     return input.replace(/\\u(\w\w\w\w)/g, function (a, b) {
//         var charcode = parseInt(b, 16);
//         return String.fromCharCode(charcode);
//     });
// }

export default class Answer6Screen extends React.Component {
  constructor() {
    super();
    this.state = {
      selectedIndexs: '', // '36270~9'
    };
  }

  async UNSAFE_componentWillMount() {
    const selectedIndexs = (await getData('a6')) || ''; // '3627'
    if (!isEmpty(selectedIndexs)) {
      this.setState({
        selectedIndexs: selectedIndexs,
      });
    }
  }

  pressItem(index) {
    // 0~9
    let selectedIndexs = this.state.selectedIndexs;
    const inclueIndex = selectedIndexs.indexOf(index.toString());
    if (inclueIndex !== -1) {
      const tempString =
        selectedIndexs.substring(0, inclueIndex) +
        selectedIndexs.substring(inclueIndex + 1, selectedIndexs.length);
      selectedIndexs = tempString;
    } else {
      selectedIndexs += index.toString();
    }

    this.setState({
      selectedIndexs: selectedIndexs,
    });
    const data = {
      screen: 6,
      save: selectedIndexs,
    };
    this.props.onChanged(data);
  }

  render() {
    const selectedIndexs = this.state.selectedIndexs;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.subcontainer}>
          <TextS1
            family="bold"
            size="16"
            color={Global.C_PERIWINKLE}
            label={subTitle}
            textstyle={styles.subtitle_text}
          />

          {items.map((item, index) => {
            const isSelected =
              selectedIndexs.indexOf(index.toString()) === -1 ? false : true;
            // const ChR = hasChR[index] ? convertUnicode('®') : '';
            const check_backcolor = isSelected
              ? Global.C_PERIWINKLE
              : C_CIRCLE_BACK_DESELECT;
            return (
              <TouchableOpacity
                key={index}
                onPress={this.pressItem.bind(this, index)}
                style={styles.item}>
                <View
                  style={[
                    styles.check_wrapper,
                    { backgroundColor: check_backcolor },
                  ]}>
                  <FontAwesomeIcon
                    name="check"
                    size={20}
                    color={isSelected ? 'white' : Global.C_PERIWINKLE_MORELIGHT}
                  />
                </View>

                <View style={styles.item_textwrapper}>
                  <TextS2
                    family="reg"
                    size="16"
                    color={Global.C_PERIWINKLE}
                    label={item}
                  />
                </View>
              </TouchableOpacity>
            );
          })}

          <View style={styles.spacer_h30} />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  subcontainer: {
    width: '100%',
    paddingHorizontal: '15%',
  },
  item: {
    marginTop: 20,
    width: Global.ScreenWidth,
    // height: ItemHeight,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  check_wrapper: {
    width: ItemHeight,
    height: ItemHeight,
    borderRadius: Math.round(ItemHeight / 2),
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  item_textwrapper: {
    width: Math.round(Global.ScreenWidth * 0.7 - ItemHeight - 15),
  },
  spacer_h30: {
    height: 30,
  },
  subtitle_text: {
    marginTop: 10,
    alignSelf: 'center',
  },
});
