import React from 'react';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { TextS0, TextS1, TextS2 } from '@component/Text';
import Global from '@utils/GlobalValue';
import AnswerImageItem from './AnswerImageItem';
const QItemImages = [
  require('@images/Q1.png'),
  require('@images/Q2.png'),
  require('@images/Q3.png'),
  require('@images/Q4.png'),
  require('@images/Q5.png'),
  require('@images/Q6.png'),
];
const QItemImageWidth = Math.round(Global.ScreenWidth * 0.14);
const QItemWidth = Math.round(QItemImageWidth * 1.6);

export default class SelectionView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      savedScreens: props.savedScreens,
      // doValidateGridView: props.doValidateGridView || false,
    };
  }

  pressItem(screenIndex) {
    this.props.onPressGridViewItem(screenIndex);
  }

  render() {
    const doValidation = this.props.doValidateGridView || false;
    const savedScreens = this.state.savedScreens;
    const missingScreenCount = 6 - savedScreens.length;
    const answers = this.props.answers;
    let groupImage = [];
    for (let i = 0; i < 3; i++) {
      let twogroup = [];
      for (let j = 0; j < 2; j++) {
        const screenIndex = i * 2 + j + 1;
        let ScreenSaved = false;
        if (savedScreens.indexOf(`${screenIndex}`) !== -1) {
          ScreenSaved = true;
        }
        const borderWidth = doValidation && !ScreenSaved ? 4 : 0;
        const imageContentData = answers[screenIndex - 1];
        twogroup.push(
          <TouchableOpacity
            onPress={this.pressItem.bind(this, screenIndex)}
            key={`${i}${j}`}
            style={[styles.item_wrapper, { borderWidth: borderWidth }]}>
            {ScreenSaved ? (
              <View style={styles.item}>
                <AnswerImageItem
                  screenIndex={screenIndex}
                  data={imageContentData}
                />
              </View>
            ) : (
                <View style={styles.h30}>
                  <TextS2
                    family="bold"
                    size="28"
                    color={'white'}
                    label={screenIndex}
                    textstyle={styles.item_number_text}
                  />
                </View>
              )}
          </TouchableOpacity>,
        );
      }
      groupImage[i] = twogroup;
    }

    return (
      <View style={styles.container}>
        <View style={styles.youranswer}>
          <TextS2
            family="bold"
            size="18"
            color={'white'}
            label={'Deine Antworten'}
          />
        </View>

        <View style={styles.items_wrapper}>
          <View style={styles.twoitems_wrapper}>{groupImage[0]}</View>

          <View style={styles.twoitems_wrapper}>{groupImage[1]}</View>

          <View style={styles.twoitems_wrapper}>{groupImage[2]}</View>
        </View>

        {doValidation ? (
          <View style={styles.missingwarning_container}>
            <View style={styles.missingwarning_wrapper}>
              <TextS1
                family="demi"
                size="14"
                color={Global.C_PERIWINKLE}
                label={`Du hast leider noch nicht alle\nFragen beantwortet`}
                textstyle={{ textAlign: 'center' }}
              />
            </View>
            <View style={styles.triangle_wrapper}>
              {/* <Image
                source={require('@images/triangle.png')}
                style={styles.wh100}
                resizeMode="contain"
              /> */}
            </View>
          </View>
        ) : (
            <View style={styles.f_02} />
          )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item_wrapper: {
    width: QItemWidth,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Math.round(QItemWidth / 2),
    borderColor: 'white',
  },
  container: {
    width: '100%',
    height: '100%',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: Global.C_PERIWINKLE_LIGHT,
  },
  youranswer: {
    flex: 0.15,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  items_wrapper: {
    flex: 0.65,
    width: '70%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  twoitems_wrapper: {
    width: '100%',
    height: QItemWidth,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  item_image: {
    width: '70%',
    height: '70%',
  },
  triangle_wrapper: {
    borderTopWidth: 10,
    borderRightWidth: 10,
    borderBottomWidth: 0,
    borderLeftWidth: 10,
    borderTopColor: 'white',
    borderRightColor: 'transparent',
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
    marginBottom: 5,
  },
  wh100: {
    width: '100%',
    height: '100%',
  },
  f_02: {
    flex: 0.2,
  },
  missingwarning_wrapper: {
    // width: '80%',
    // height: 40,
    backgroundColor: 'white',
    borderRadius: 5,
    paddingVertical: 10,
    paddingHorizontal: 20,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  missingwarning_container: {
    flex: 0.2,
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  item: {
    width: '80%',
    height: '80%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  h30: {
    height: 30,
  },
  item_number_text: {
    lineHeight: 30,
  },
});
