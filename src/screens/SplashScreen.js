import React from 'react';
import { View, Image, StatusBar, StyleSheet } from 'react-native';
import Global from '@utils/GlobalValue';

const DELAY_SECONDS = 2000;

export default class SplashScreen extends React.Component {
  UNSAFE_componentWillMount() {
    setTimeout(() => {
      this.props.navigation.navigate('App');
    }, DELAY_SECONDS);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />

        <View style={styles.logo_wrapper}>
          <Image
            source={require('@images/logo.png')}
            style={styles.logo_image}
            resizeMode="contain"
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Global.C_PERIWINKLE,
  },
  logo_wrapper: {
    width: '70%',
    aspectRatio: 1.8,
  },
  logo_image: {
    width: '100%',
    height: '100%',
  },
});
